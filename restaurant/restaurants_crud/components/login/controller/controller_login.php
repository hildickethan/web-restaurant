<?php 
	$path = $_SERVER['DOCUMENT_ROOT'] . '/web-restaurant/restaurant/restaurants_crud/';
    include($path . "components/login/model/DAO_login.php");
	//@session_start();
	
    switch ($_GET['op']) {
        case 'register':
            try{
                $daologin = new DAOlogin();

                //username check
                $usercheck = $daologin->getUser($_POST['usernameregister']);
                // error_log(print_r($usercheck, 1));

                if ($usercheck->num_rows > 0){
                    echo json_encode('Username already exists');
                } else {
                    //email check
                    $emailcheck = $daologin->checkEmail($_POST['emailregister']);

                    if ($emailcheck->num_rows > 0){
                        echo json_encode('Email already exists');
                    } else {
                        $rdo = $daologin->register($_POST);
                        
                        if(!$rdo){
                            $callback = 'index.php?page=503';
                            die('<script>window.location.href="'.$callback .'";</script>');
                        }
                        else{
                            echo json_encode('Inserted');
                        }
                    }
                }
            }catch (Exception $e){
                $callback = 'index.php?page=503';
                die('<script>window.location.href="'.$callback .'";</script>');
            }
            
            break;

        case 'login':
            try{
                $daologin = new DAOlogin();

                //username check
                $usercheck = $daologin->getUser($_POST['usernamelogin']);

                if ($usercheck->num_rows > 0){
                    $user = $usercheck->fetch_assoc();
                    if(password_verify($_POST['passwordlogin'], $user['password'])){
                        session_start();
                        $_SESSION['user'] = $user;
                    } else {
                        echo json_encode('badpw');
                    }
        
                } else {
                    echo json_encode('nouser');
                }
            }catch (Exception $e){
                $callback = 'index.php?page=503';
                die('<script>window.location.href="'.$callback .'";</script>');
            }
            break;

        case 'checklogin':
            session_start();
            if (isset($_SESSION['user'])) {
                echo json_encode($_SESSION['user']);
            } else {
                echo json_encode("notlogged");
                }
            break;

        case 'logout':
            @session_start();
            unset($_SESSION['user']);
            session_destroy();

            break;

        case 'activity':
            session_start();
            if (isset($_SESSION['user'])) {
                if (isset($_SESSION["time"])) {  
                    if((time() - $_SESSION["time"]) >= 120) {
                        echo true; 
                        exit();
                    }
                }
            }
            break;

		default:
			include("view/inc/error404.php");
			break;
	}
