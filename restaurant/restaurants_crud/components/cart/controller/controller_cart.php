<?php 
	$path = $_SERVER['DOCUMENT_ROOT'] . '/web-restaurant/restaurant/restaurants_crud/';
    include($path . "components/cart/model/DAO_cart.php");
	//@session_start();
    switch ($_GET['op']) {
        case 'list':
                include($path . "components/cart/view/cart.html");
            break;
        
        case 'price':
            try{
                $daocart = new DAOCart();
                $rdo = $daocart->get_price($_POST);
            }catch (Exception $e){
                $callback = 'index.php?page=503';
                die('<script>window.location.href="'.$callback .'";</script>');
            }
            
            if(!$rdo){
                $callback = 'index.php?page=503';
                die('<script>window.location.href="'.$callback .'";</script>');
            }
            else{
                $dinfo = array();
                foreach ($rdo as $row) {
                    array_push($dinfo, $row);
                }
                echo json_encode($dinfo);
            }
            break;

        case 'purchase':
            try{
                $daocart = new DAOCart();

                $cart = $_POST['restaurants'];

                $pid = $daocart->get_pid();
                $pid = $pid->fetch_assoc();

                // first purchase in DB
                if ($pid['pid'] == null)
                    $pid['pid'] = 0;
                
                $pid['pid']++;

                foreach ($cart as $key => $restaurant) {
                    $price = $daocart->get_price($restaurant);
                    $price = $price->fetch_assoc();
                    $restaurant['price'] = $price['price'];

                    $purchase = $daocart->purchase($restaurant, $pid);
                }

            }catch (Exception $e){
                $callback = 'index.php?page=503';
                die('<script>window.location.href="'.$callback .'";</script>');
            }
            
            if(!$purchase){
                $callback = 'index.php?page=503';
                die('<script>window.location.href="'.$callback .'";</script>');
            }
            else{
                echo json_encode('success');
            }
            break;
		default:
			include("view/inc/error404.php");
			break;
	}
