$(document).ready(function(){

    ////////////////////////////////////////////////
    // Initial checks
    ////////////////////////////////////////////////

    try {
        var type = document.getElementById('searchtype').value;
    } catch (error) {
        // todo
    }

    if (getQueryVariable('page') == 'controller_shop' && getQueryVariable('op') == 'list'){
        restaurantSearch();
    }

    if (getQueryVariable('page') == 'controller_shop' && getQueryVariable('op') == 'details'){
        loadRestaurant();
    }

    ////////////////////////////////////////////////
    // Functions
    ////////////////////////////////////////////////
    function getQueryVariable(variable){

        var query = window.location.search.substring(1);
        var vars = query.split("&");
        for (var i=0;i<vars.length;i++) {
                var pair = vars[i].split("=");
                if(pair[0] == variable){return pair[1];}
        }
        return(false);
    }

    function restaurantSearch(){
        
        var searchdata = {
            fields: {
                "type": {
                    "typed": type
                },
                "tastes": {
                    "typed": document.getElementById('searchtastes').value
                },
                "name": {
                    "typed": document.getElementById('searchname').value
                }
            }
        };

        $.ajax({
            type: 'POST',
            url: 'components/shop/controller/controller_shop.php?op=search',
            data: searchdata,
            success: function(data) {
                data = JSON.parse(data);
                // console.log(data);
                document.getElementById('restaurants').innerHTML="";

                var i = 1;
                $.each(data, function(index, restaurant){
                    // var temp = 
                    //     `<div class ='col-md-4 feature' id='restaurant_${data[index].id}' name='restaurant'>`+
                    //     `<img src='view/img/restaurant${i}.jpg' alt='No image available'>`+
                    //     "<h4>"+data[index].name+"</h4>"+
                    //     "</div>";
                    
                    // $('#restaurants').append(temp);
                    
                    var r = document.createElement("div");
                    r.classList.add("col-md-4","feature");
                    
                    var temp = 
                        `<img src='view/img/restaurant${i}.jpg' alt='No image available'>`+
                        `<h4>${data[index].name}</h4>`+
                        '<a type="button" class="site-btn sb-c3">+</a>';
                    
                    if (i < 3)
                        i++;
                    else
                        i = 1;

                    r.innerHTML = (temp);

                    r.childNodes[0].addEventListener("click", function(){

                        $.ajax({
                            data: data[index],
                            url: "components/shop/controller/controller_shop.php?op=redirectdetails",
                            type: 'POST',
                            success: function(data){
                                // console.log(data);
                                
                                window.location.href ="index.php?page=controller_shop&op=details";
                            }
                        });
                    });

                    r.childNodes[2].addEventListener('click', function(){
                        if (localStorage && localStorage.getItem('cart')) {
                            var exists = false;
                            // console.log(quantity);
                            var cart = JSON.parse(localStorage.getItem('cart'));
                            cart.restaurants.forEach(element => {
                                if (element.id == data[index].id){
                                    exists = true;
                                }
                                // break?
                            }); 
                        } else {
                            var cart = {};
                            cart.restaurants = [];
                            localStorage.setItem('cart', JSON.stringify(cart));
                        }

                        data[index].quantity = 1; 

                        if (exists){
                            cart.restaurants.forEach(element => {
                                if (element.id == data[index].id){
                                    element.quantity++;
                                }
                                // break?
                            }); 
                        } else {
                            cart['restaurants'].push(data[index]);
                        }

                        localStorage.setItem('cart', JSON.stringify(cart));
                        $('#cart-number').html(cart.restaurants.length);
                    });
                    
                    $('#restaurants').append(r);

                });
            }
        });

        ////////////////////////////////////////////////
        // API
        ////////////////////////////////////////////////

        var api_promise = new Promise(function(resolve, reject) {
            var json = null;
            $.ajax({
                type: 'GET',
                url: "model/token.json",
                dataType: 'JSON',
                success: function (data) {
                    json = data;
                }
            });
            setTimeout(function() {
            resolve(json);
            }, 200);
        });
        
        api_promise.then(function(tokens) {
            // console.log(value);
            $.ajax({
                type: 'GET',
                url: 'https://cors-anywhere.herokuapp.com/https://api.yelp.com/v3/businesses/search?location=valenciancommunity&categories=restaurants&limit=10',
                headers: {
                    'Authorization':`Bearer ${tokens.keys.yelp_api}`,
                },
                dataType: 'JSON',
                success: function(data) {
                    // console.log(data);

                    $.each(data.businesses, function(index, restaurant){
                        // var temp = 
                        //     `<div class ='col-md-4 feature' id='api_${data.businesses[index].id} name='api'>`+
                        //     `<img src='${data.businesses[index].image_url}' alt='No image available'>`+
                        //     `<h4>${data.businesses[index].name}</h4>`+
                        //     "</div>";

                        // $('#restaurants').append(temp);
                        
                        var r = document.createElement("div");
                        r.classList.add("col-md-4","feature");
                        
                        var temp = 
                            `<img src='${data.businesses[index].image_url}' alt='No image available'>`+
                            `<h4>${data.businesses[index].name}</h4>`;
                    

                        r.innerHTML = (temp);
                        
                        r.addEventListener("click", function(){
                            var details = data.businesses[index];
                            
                            $.ajax({
                                data: details,
                                url: "components/shop/controller/controller_shop.php?op=redirectdetails",
                                type: 'POST',
                                success: function(data){
                                    data = JSON.parse(data);
                                    // console.log(data);
                                    
                                    window.location.href ="index.php?page=controller_shop&op=details";
                                }
                            });
                        });
                        
                        $('#restaurants').append(r);
                    });
                },
                error: function(e) {
                    console.log('ERROR '+e.status);
                    console.log(e.responseText);
                }
            });
        });

    }

    function removeChildren(inputbox) {
        //remove old
        while (inputbox.firstChild) {
            inputbox.removeChild(inputbox.firstChild);
        }
    }

    function loadRestaurant() {
        // get clicked restaurant
        $.ajax({
            url: "components/shop/controller/controller_shop.php?op=loaddetails",
            type: 'GET',
            success: function(data){
                data = JSON.parse(data);
                // console.log(data);

                // bandaid fix for DB vs API restaurants, unstable
                // could be function instead of copy paste code
                if (data.id.length < 5) {
                    document.getElementById('detailsname').textContent = data.name;
                    document.getElementById('details1').textContent = `Type: ${data.type}`;
                    document.getElementById('details2').textContent = `People: ${data.people}`;
                    document.getElementById('details3').textContent = `Date: ${data.selected_date}`;
                    document.getElementById('details4').textContent = `Tastes: ${data.tastes}`;
                } else {
                    document.getElementById('detailsimg').setAttribute('src', data.image_url)
                    document.getElementById('detailsname').textContent = data.name;
                    document.getElementById('details1').textContent = `Location: ${data.location.display_address.toString()}`;
                    document.getElementById('details2').textContent = `Phone: ${data.display_phone}`;
                    document.getElementById('details3').textContent = `Rating: ${data.rating}`;
                    document.getElementById('details4').innerHTML = `<a href=${data.url}>Restaurant on Yelp</a>`;

                }
            }
        });
    }    

    ////////////////////////////////////////////////
    // onclick
    ////////////////////////////////////////////////
    

    $('#searchtype').on('click', function(){
        type = document.getElementById('searchtype').value;
    });

    $('#searchname, #searchtastes').on('click', function(){
        removeChildren(document.getElementById('dropdown-name'));
        removeChildren(document.getElementById('dropdown-tastes'));
    });

    $('#searchbutton_shop').on('click', function(){
        restaurantSearch();
    });

    $('#searchbutton_home').on('click', function(){
        var searchdata = {
            fields: {
                "type": {
                    "typed": type
                },
                "tastes": {
                    "typed": document.getElementById('searchtastes').value
                },
                "name": {
                    "typed": document.getElementById('searchname').value
                }
            }
        };
        
        $.ajax({
            data: searchdata,
            url: "components/shop/controller/controller_shop.php?op=redirectlist",
            type: 'POST',
            success: function(data){
                // console.log(data);
                window.location.href ="index.php?page=controller_shop&op=list";
            }
        });
    });

    ////////////////////////////////////////////////
    // Keyup / Autocomplete
    ////////////////////////////////////////////////

    $('#searchname, #searchtastes').on('keyup', function(){
        
        var fielddata = {
            "name": this.value,
            "field": this.id.substring(6), //all ids are "searchX"
            "dropdown": "dropdown-"+this.id.substring(6),
            "type": type
        };
        // console.log(this.value);
        // console.log(value.name != "");
        // console.log(fielddata);

        //no suggestions if nothing is input
        inputbox = document.getElementById(fielddata.dropdown);
        removeChildren(inputbox);

        if (fielddata.name != ""){
            $.ajax({
                type: 'POST',
                url: 'components/shop/controller/controller_shop.php?op=autocomplete',
                data: fielddata,
                success: function(data) {
                    // console.log(data);
                    data = JSON.parse(data);
                    
                    // console.log(data);

                    //show the div
                    if (!inputbox.classList.contains("show")) {
                        inputbox.classList.toggle("show");
                    }

                    //fill out the div
                    $.each(data, function(index){
                        var node = document.createElement("a");                  
                        switch (fielddata.field) {
                            case 'name':
                                node.appendChild(document.createTextNode(data[index].name));    
                            break;

                            case 'tastes':
                                node.appendChild(document.createTextNode(data[index].tastes));    
                            break;

                            default:
                                break;
                        }  
                        inputbox.appendChild(node);

                        
                        node.addEventListener("click", function(){
                            document.getElementById('search'+fielddata.field).value = node.text;
                            removeChildren(inputbox);
                        });
                    });
                }
            });
        }

        //console.log(this.id);
    });
});