-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Mar 12, 2019 at 05:45 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 5.6.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `restaurant`
--

-- --------------------------------------------------------

--
-- Table structure for table `purchases`
--

CREATE TABLE `purchases` (
  `pid` int(11) NOT NULL,
  `rid` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `price` float NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchases`
--

INSERT INTO `purchases` (`pid`, `rid`, `uid`, `price`, `quantity`) VALUES
(1, 3, 4, 100, 4),
(1, 4, 4, 0, 1),
(1, 5, 4, 0, 1),
(2, 3, 4, 100, 4);

-- --------------------------------------------------------

--
-- Table structure for table `restaurants`
--

CREATE TABLE `restaurants` (
  `id` int(11) NOT NULL,
  `name` varchar(60) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL,
  `people` int(11) DEFAULT NULL,
  `selected_date` date DEFAULT NULL,
  `tastes` varchar(60) DEFAULT NULL,
  `price` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `restaurants`
--

INSERT INTO `restaurants` (`id`, `name`, `type`, `people`, `selected_date`, `tastes`, `price`) VALUES
(3, 'sss', 'Gourmet', 2, '0000-00-00', 'Meat', 100),
(4, 'aaa', 'Gourmet', 5, '0000-00-00', 'Meat', 0),
(5, 'sdsdffsd', 'Fast Food', 1, '0000-00-00', 'Meat,Fish', 0),
(6, 'sdsdffsd', 'Fast Food', 1, '0000-00-00', 'Meat,Fish', 0),
(7, 'sdsdffsd', 'Fast Food', 1, '0000-00-00', 'Meat,Fish', 0),
(41, 'sdsdffsd', 'Gourmet', 3, '2018-12-27', 'Fish', 0),
(42, 'sdsdffsd', 'Gourmet', 3, '2018-12-27', 'Fish', 0),
(43, 'sdsdffsd', 'Gourmet', 3, '2018-12-27', 'Fish', 0),
(44, 'aaaa', 'Gourmet', 2, '2018-12-19', 'Meat,Fish', 0),
(46, 'asdas', 'Gourmet', 2, '2019-01-09', 'Meat', 0),
(48, 'asdasd', 'Gourmet', 5, '2019-01-01', 'Meat,Vegetarian', 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `admin` tinyint(1) NOT NULL,
  `username` varchar(40) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(40) NOT NULL,
  `avatar` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `admin`, `username`, `password`, `email`, `avatar`) VALUES
(1, 0, 'aaaaaa', '$2y$10$H8KtiVMZJAQsWtfNqswQL.Pn5r6DY49fZqnhumaqp//v.JOKSHgQC', 'aaaaaa@gmail.com', 'https://api.adorable.io/avatars/256/aaaaaa'),
(2, 0, 'bbbbbbb', '$2y$10$NKFNuUZTErZB6k9Ybw32NevsWEX67isW6hIHZ7veRmQKm3QlDpisK', 'bbbb@bbb.com', 'https://api.adorable.io/avatars/256/bbbbbbb'),
(3, 0, 'asdasda', '$2y$10$qTmNN708U9Cs5qiP0cFJCe8Beol8N7eOQm4XSq2DReg2/H8MVYemq', 'aaaa@aaaa.com', 'https://api.adorable.io/avatars/256/asdasda'),
(4, 1, 'jordilg13', '$2y$10$3ELOciiqbeyG/N2/l.i.zeFX8PPdmGwZmY9Q6sUDnXX8apCRWEcuq', 'jordilg13@jordilg.com', 'https://api.adorable.io/avatars/256/jordilg13'),
(5, 0, 'jordilg13a', '$2y$10$.Vf094dKNcYIdJOn9jAJ/.BrZXWXiHa7CY6vuolt9Ey7hTmDthLTW', 'aaaaaaa@aaaaa.com', 'https://api.adorable.io/avatars/256/jordilg13a');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `restaurants`
--
ALTER TABLE `restaurants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `restaurants`
--
ALTER TABLE `restaurants`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
