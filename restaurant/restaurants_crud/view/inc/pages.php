<?php
	//print_r($_GET);
	if(!isset($_GET['page'])){
		include("module/home/controller/controller_home.php");
	} else{
		switch($_GET['page']){
			case "homepage";
				include("module/home/controller/controller_home.php");
				break;
			case "controller_restaurants";
				include("module/restaurants/controller/".$_GET['page'].".php");
				break;
			case "controller_favourites";
				include("module/favourites/controller/".$_GET['page'].".php");
				break;
			case "controller_shop";
				include("components/shop/controller/".$_GET['page'].".php");
				break;
			case "controller_cart";
				include("components/cart/controller/".$_GET['page'].".php");
				break;
			case "services";
				include("module/services/".$_GET['page'].".php");
				break;
			case "aboutus";
				include("module/aboutus/".$_GET['page'].".php");
				break;
			case "contactus";
				include("module/contact/controller/".$_GET['page'].".php");
				break;
			case "controller_login";
				include("components/login/controller/".$_GET['page'].".php");
				break;
			case "404";
				include("view/inc/error".$_GET['page'].".php");
				break;
			case "503";
				include("view/inc/error".$_GET['page'].".php");
				break;
			default;
				include("module/home/controller/controller_home.php");
				break;
		}
	}
?>