<?php
    $path = $_SERVER['DOCUMENT_ROOT'] . '/web-restaurant/restaurant/restaurants_crud/';
    include($path . "module/restaurants/model/DAORestaurants.php");
    @session_start();
    
    if (isset($_SESSION['user'])){
        if (!$_SESSION['user']['admin']){
            die('<script>window.location.href="index.php";</script>');
        }
    }else {
        die('<script>window.location.href="index.php";</script>');
    }
    
    //print_r($_GET);
    switch($_GET['op']){
        case 'list';
            try{
                $daorestaurants = new DAORestaurants();
                $rdo = $daorestaurants->select_all_restaurants();
            }catch (Exception $e){
                $callback = 'index.php?page=503';
			    die('<script>window.location.href="'.$callback .'";</script>');
            }
            
            if(!$rdo){
    			$callback = 'index.php?page=503';
			    die('<script>window.location.href="'.$callback .'";</script>');
    		}else{
                include("module/restaurants/view/list_restaurants.php");
    		}
            break;
            
        case 'create';
            include("module/restaurants/model/validate.php");
            
            if (isset($_POST['Name'])){
                $exists=validate($_POST['id']);
                
                if (!$exists){
                    $_SESSION['restaurant']=$_POST;
                    try{
                        $daorestaurants = new DAORestaurants();
    		            $rdo = $daorestaurants->insert_restaurant($_POST);
                    }catch (Exception $e){
                        $callback = 'index.php?page=503';
        			    die('<script>window.location.href="'.$callback .'";</script>');
                    }
                    
		            if($rdo){
            			echo '<script language="javascript">alert("Correctly saved in database")</script>';
            			$callback = 'index.php?page=controller_restaurants&op=list';
        			    die('<script>window.location.href="'.$callback .'";</script>');
            		}else{
            			$callback = 'index.php?page=503';
    			        die('<script>window.location.href="'.$callback .'";</script>');
            		}
                }
                else
                    echo '<script language="javascript">alert("Restaurant already exists")</script>';
            }
            include("module/restaurants/view/create_restaurants.php");
            break;
            
        case 'update';
            include("module/restaurants/model/validate.php");
            
            if (isset($_POST['id'])){
                //$check=validate($_POST['id']);
                
                $check = false;
                if (!$check){
                    $_SESSION['restaurant']=$_POST;
                    try{
                        $daorestaurants = new DAORestaurants();
    		            $rdo = $daorestaurants->update_restaurant($_POST);
                    }catch (Exception $e){
                        $callback = 'index.php?page=503';
        			    die('<script>window.location.href="'.$callback .'";</script>');
                    }
                    
		            if($rdo){
            			echo '<script language="javascript">alert("Correctly updated in the database")</script>';
            			$callback = 'index.php?page=controller_restaurants&op=list';
        			    die('<script>window.location.href="'.$callback .'";</script>');
            		}else{
            			$callback = 'index.php?page=503';
    			        die('<script>window.location.href="'.$callback .'";</script>');
            		}
                }
                else
                    echo '<script language="javascript">alert("Restaurant already exists")</script>';
            }
            
            try{
                $daorestaurants = new DAORestaurants();
            	$rdo = $daorestaurants->select_restaurant($_GET['id']);
            	$restaurant=get_object_vars($rdo);
            }catch (Exception $e){
                $callback = 'index.php?page=503';
			    die('<script>window.location.href="'.$callback .'";</script>');
            }
            
            if(!$rdo){
    			$callback = 'index.php?page=503';
    			die('<script>window.location.href="'.$callback .'";</script>');
    		}else{
        	    include("module/restaurants/view/update_restaurant.php");
    		}
            break;
            
        case 'read';
            try{
                $daorestaurants = new DAORestaurants();
            	$rdo = $daorestaurants->select_restaurant($_GET['id']);
            	$restaurant=get_object_vars($rdo);
            }catch (Exception $e){
                $callback = 'index.php?page=503';
			    die('<script>window.location.href="'.$callback .'";</script>');
            }
            if(!$rdo){
    			$callback = 'index.php?page=503';
    			die('<script>window.location.href="'.$callback .'";</script>');
    		}else{
                include("module/restaurants/view/read_restaurant.php");
    		}
            break;
            
        case 'delete';
            //print_r($_POST);
            if (isset($_POST['delete'])){
                try{
                    $daorestaurants = new DAOrestaurants();
                	$rdo = $daorestaurants->delete_restaurant($_GET['id']);
                }catch (Exception $e){
                    $callback = 'index.php?page=503';
    			    die('<script>window.location.href="'.$callback .'";</script>');
                }
            	
            	if($rdo){
        			echo '<script language="javascript">alert("Correctly deleted from database")</script>';
        			$callback = 'index.php?page=controller_restaurants&op=list';
    			    die('<script>window.location.href="'.$callback .'";</script>');
        		}else{
        			$callback = 'index.php?page=503';
			        die('<script>window.location.href="'.$callback .'";</script>');
        		}
            }
            
            include("module/restaurants/view/delete_restaurant.php");
            break;
        case 'deleteall';
            if (isset($_POST['deleteall'])){
                try{
                    $daorestaurants = new DAOrestaurants();
                    $rdo = $daorestaurants->delete_all_restaurants();
                }catch (Exception $e){
                    $callback = 'index.php?page=503';
                    die('<script>window.location.href="'.$callback .'";</script>');
                }
                
                if($rdo){
                    echo '<script language="javascript">alert("All restaurants deleted successfully")</script>';
                    $callback = 'index.php?page=controller_restaurants&op=list';
                    die('<script>window.location.href="'.$callback .'";</script>');
                }else{
                    $callback = 'index.php?page=503';
                    die('<script>window.location.href="'.$callback .'";</script>');
                }
            }
        
            include("module/restaurants/view/delete_all_restaurants.php");
            break;
        case 'read_modal';
            
            try{
                $daorestaurants = new DAORestaurants();
                $rdo = $daorestaurants->select_restaurant($_GET['modal']);
            }catch (Exception $e){
                echo json_encode("error");
                exit;
            }
            
            if(!$rdo){
                echo json_encode("error");
                exit;
            }else{
                $restaurant=get_object_vars($rdo);
                echo json_encode($restaurant);
                //echo json_encode("error");
                exit;
            }

            break;

        default;
            include("view/inc/error404.php");
            break;
    }