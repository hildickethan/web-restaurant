// Name
function validate_name(name) {
    if (name.length > 0) {
        var regexp = /^([a-zA-Z]+){2}$/;
        return regexp.test(name);
    }
    return false;
}
// Type
function validate_type(type) {
    if (type.length > 0) {
        var regexp = /^Gourmet|Fast Food|Ethnic$/;
        return regexp.test(type);
    }
    return false;
}
// Date
function validate_date(date) {
    if (date.length > 0) {
        var regexp = /^[12][90][0-9][0-9]\-[01]?[0-9]\-[0-3]?[0-9]$/;
        return regexp.test(date);
    }
    return false;
}

function getPeople() {
    var people = "";

    if (document.getElementById('p1').checked)
        people = document.getElementById('p1').value;
    if (document.getElementById('p2').checked)
        people = document.getElementById('p2').value;
    if (document.getElementById('p3').checked)
        people = document.getElementById('p3').value;
    if (document.getElementById('p4').checked)
        people = document.getElementById('p4').value;
    if (document.getElementById('p5').checked)
        people = document.getElementById('p5').value;
    if (document.getElementById('p6').checked)
        people = document.getElementById('p6').value;
    
    return people;
}

function getTastes() {
    var tastes = "";
    
    if (document.getElementById('t1').checked)
        tastes += document.getElementById('t1').value+" ";
    if (document.getElementById('t2').checked)
        tastes += document.getElementById('t2').value+" ";
    if (document.getElementById('t3').checked)
        tastes += document.getElementById('t3').value;
    
    return tastes;
}

function validate_restaurant() {
    var result = true;

    var name = document.getElementById('name').value;
    var type = document.getElementById('type').value;
    var date = document.getElementById('datepicker').value;
/*
    console.log(name);
    console.log(type);
    console.log(people);
    console.log(date);
    console.log(tastes);
*/  
    if (!validate_name(name)) {
        document.getElementById('e_name').innerHTML = '<span class="error">Not a valid name</span>';
        result = false;
    } else
        document.getElementById('e_name').innerHTML = "";

    if (!validate_type(type)) {
        document.getElementById('e_type').innerHTML = '<span class="error">Not a valid type</span>';
        result = false;
    } else
        document.getElementById('e_type').innerHTML = "";
    
    if (getPeople() === "") {
        document.getElementById('e_people').innerHTML = '<span class="error">Select an amount of people</span>';
        result = false;
    } else
        document.getElementById('e_people').innerHTML = "";

    if (!validate_date(date)) {
        document.getElementById('e_date').innerHTML = '<span class="error">Not a valid date</span>';
        result = false;
    } else
        document.getElementById('e_date').innerHTML = "";

    if (getTastes() === "") {
        document.getElementById('e_tastes').innerHTML = '<span class="error">Select at least one</span>';
        result = false;
    } else
        document.getElementById('e_tastes').innerHTML = "";
        
    console.log(result)
    if(result){
        try {
            document.formrestaurants.submit();
            document.formrestaurants.action="";
        } catch (error) {
            document.aupdate_restaurant.submit();
            document.aupdate_restaurant.action="";
        }
    }
}