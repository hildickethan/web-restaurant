<?php 
	$path = $_SERVER['DOCUMENT_ROOT'] . '/web-restaurant/restaurant/restaurants_crud/';
    include($path . "module/favourites/model/DAO_favourites.php");
	@session_start();
	if (!isset($_SESSION['user'])){
        die('<script>window.location.href="index.php";</script>');
    }
    switch ($_GET['op']) {
		case 'list':
				include("module/favourites/view/list_favourites.php");
			break;
			
		case 'datatable':
            try{
				$daofavourites = new DAOfavourites();
				$rlt = $daofavourites->select_favourites();
			} catch(Exception $e){
				echo json_encode("error");
			}

			if(!$rlt){
				echo json_encode("error");
			}
			else{
				$dinfo = array();
				foreach ($rlt as $row) {
					array_push($dinfo, $row);
				}
				echo json_encode($dinfo);
			}
			break;
			
		default:
			include("view/inc/error404.php");
			break;
	}
