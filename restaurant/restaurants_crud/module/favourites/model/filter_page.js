$(document).ready(function () {
    var url = "module/favourites/controller/controller_favourites.php?op=datatable";  
    // prepare the data
    var source =
    {
        dataType: "json",
        dataFields: [
            { name: 'id', type: 'integer' },
            { name: 'name', type: 'string' },
            { name: 'selected_date', type: 'string' }
        ],
        id: 'id',
        url: url
    };
    
    var dataAdapter = new $.jqx.dataAdapter(source);
    $("#dataTable").jqxDataTable(
    {
        width: $("#dataTable").width(),
        theme: 'metro',
        pagerButtonsCount: 10,
        source: dataAdapter,
        sortable: true,
        pageable: true,
        altRows: true,
        filterable: true,
        columnsResize: true,
        pagerMode: 'advanced',
        columns: [
          { text: 'ID', dataField: 'id', width: 50 },
          { text: 'Name', dataField: 'name', width: 350 },
          { text: 'Date', dataField: 'selected_date'}
      ]
    });  
});
